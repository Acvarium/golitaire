extends Sprite
export var id = 0
export var to_save = true
var global
var main_node
var eColor = Color(0,1,0)
var nColor = Color(1,1,1)
export var fExist = false

func _ready():
	global = get_node("/root/global")
	main_node = get_node("/root/main")
	$Label.text = str(id)

func _on_Button_pressed():
	if to_save:
		global.save_game(id)
		main_node.hide_folsd()
		check_file()
	else:
		if !global.fileExists[id]:
			main_node.hide_folsd()
			return
		global.load_from = id
		get_tree().reload_current_scene()
	
func check_file():
	if !global.fileExists[id]:
		$Label.modulate = nColor
	else:
		$Label.modulate = eColor
