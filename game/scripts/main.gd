extends Control
var list = []
var cardSize = Vector2(96,144)
var selectedCard = null
var pickedCards = []
var prevClosest = null
export var allowe_on_table = false
export var allowe_all = false
export var toAutoComptlite = false
var lastPressTime = 0.0
var time_step = 200
var global
var allowe_victory = true
var is_save_fold = true
var is_load_fold = true

func _ready():
	global = get_node("/root/global")
	set_physics_process(false)
	global.load_from = -1
	$version.text = global.version
	
func _physics_process(delta):
	if toAutoComptlite:
		if (OS.get_ticks_msec() - lastPressTime) > time_step:
			lastPressTime = OS.get_ticks_msec()
			var card = null
			for i in range(7):
				card = cardOnTop(get_node("row" + str(i+1)))
				if !card.place:
					if autoMove(card):
						return
			toAutoComptlite = false
			set_physics_process(false)

func findClosest(card):
	var dist = 999999.0
	var closest = null
	var pos = card.global_position
	for i in range(list.size()):
		if !allowe_all:
			if card.snapped:
				if card.snapped == list[i]:
					continue
			if list[i].snapped:
				continue
			if !list[i].face_up:
				continue
			if pickedCards.find(list[i]) != -1:
				continue
			if !checkRules(card, list[i]):
				continue
				
		var di = pos - list[i].global_position
		if abs(di.x) > cardSize.x:
			continue
		if abs(di.y) > cardSize.y:
			continue
		var d = pos.distance_to(list[i].global_position)
		if card != list[i]:
			if d < dist:
				closest = list[i]
				dist = d
	if (closest):
		$debug/Label.text = closest.name
	if prevClosest != closest:
		if prevClosest:
			prevClosest.highlight(false)
		if closest:
			closest.highlight(true)
	prevClosest = closest
	if closest:
		$debug/w2.text = closest.name
	return closest

func remove_face_down():
	for i in range(list.size(), 0, -1):
		if !list[i-1].face_up:
			list.remove(i-1)

func checkVictory():
	if $deck.snapped or $unfold.snapped:
		return false
	for i in range(7):
		if get_node("row" + str(i+1)).snapped:
			return false
	$vAnim.play("vic")
	return true
	
func flip_from(_card, flipTo):
	pickedCards = []
	var card_list = []
	var _selected = _card
	card_list.append(_selected)
	while _selected.snapped:
		_selected = _selected.snapped
		card_list.append(_selected)
	allowe_all = true
	addToList(flipTo)
	var pos = flipTo.global_position
	for i in range(card_list.size(), 0, -1):
		var c = card_list[i - 1]
		c.placedOn = null
		c.snapped = null
		c.openCard(false)
		c.global_position = pos
		c.find_place()
		pos = c.global_position
		addToList(c)
	allowe_all = false

func autoMove(card):
	for i in range(4):
		var ace = get_node("ace" + str(i))
		var onTop = cardOnTop(ace)
		if onTop:
			if checkRules(card, onTop):
				moveCardTo(card, onTop)
				return true
	return false
	
func ifAllOpened():
	if $deck.snapped or $unfold.snapped:
		return 
	for i in range(6):
		var r = get_node("row" + str(i+1))
		if r.snapped:
			if !r.snapped.face_up:
				return 
	toAutoComptlite = true
	lastPressTime = OS.get_ticks_msec()
	set_physics_process(true)
	
func cardOnTop(card):
	if !card.snapped:
		return card
	var _selected = card
	while _selected.snapped:
		_selected = _selected.snapped
	return _selected

func moveCardTo(card, place):
	allowe_all = true
	if card.placedOn:
		card.placedOn.snapped = null
		addToList(card.placedOn)
#	card.placedOn = null
	card.snapped = null
	card.global_position = place.global_position
	card.find_place()
	addToList(card)
	allowe_all = false

func clearLast():
	if prevClosest:
		prevClosest.highlight(false)
		prevClosest = null

func addToList(card):
	if list.find(card) != -1:
		return
	list.append(card)
	var ss = ""
	for c in list:
		ss += c.name + "\n"
	$debug/w.text = ss
	
func delFromList(card):
	if list.find(card) != -1:
		list.remove(list.find(card))
	var ss = ""
	for c in list:
		ss += c.name + "\n"
	$debug/w.text = ss

func checkRules(card, place):
	if allowe_all:
		return true
	if place.ruleID == 0:
		var cardRed = (card.suit == 1 or card.suit == 2)
		var placeRed = (place.suit == 1 or place.suit == 2)
		if place.place and card.card == 11:
			return true
		if cardRed != placeRed:
			if place.card - card.card == 1:
				return true
		return false
	elif place.ruleID == 1:
		if place.place:
			if card.card == 12:
				return true
			else:
				return false
		if place.suit == card.suit:
			if place.card == 12 and card.card == 0:
				return true
			if card.card - place.card == 1:
				return true
		return false
	elif place.ruleID == 2:
		if card.ruleID == 2:
			return true
	return false

func change_h(ch, par):					
	var ppos = ch.global_position		
	if (ch.get_parent() != null):
		ch.get_parent().remove_child(ch)	
	par.add_child(ch)					
	ch.global_position = ppos			

func _on_load_pressed():
	global.load_from = 0
	get_tree().reload_current_scene()

func _on_save_fold_pressed():
	for l in $save_fold.get_children():
		if l.name != "anim":
			l.check_file()
	if is_save_fold:
		$save_fold/anim.play("unfold")
	else:
		$save_fold/anim.play_backwards("unfold")
	is_save_fold = !is_save_fold

func _on_load_fold_pressed():
	for l in $load_fold.get_children():
		if l.name != "anim":
			l.check_file()
	if is_load_fold:
		$load_fold/anim.play("unfold")
	else:
		$load_fold/anim.play_backwards("unfold")
	is_load_fold = !is_load_fold

func hide_folsd():
	if !is_save_fold:
		$save_fold/anim.play_backwards("unfold")
	if !is_load_fold:
		$load_fold/anim.play_backwards("unfold")
	is_save_fold = true
	is_load_fold = true

func _on_hide_folds_pressed():
	hide_folsd()

func _on_restartButton_pressed():
	get_tree().reload_current_scene()
